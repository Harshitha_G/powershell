﻿
function Get-TargetResource
{
	[CmdletBinding()]
	[OutputType([System.Collections.Hashtable])]
	param
	(
		[ValidateSet("Present", "Absent")]
		[string]$Ensure = "Present",

		[Parameter(Mandatory)]
		[ValidateNotNullOrEmpty()]
		[string]$Name,

		[Parameter(Mandatory)]
		[ValidateNotNullOrEmpty()]
		[string]$PhysicalPath,

		[ValidateSet("Started", "Stopped")]
		[string]$State = "Started",

		[string]$ApplicationPool,

		[string]$BindingInfo,

		[string]$Protocol
	)

	$getTargetResourceResult = $null

	if(!(Get-Module -ListAvailable -Name DemoIISWebsite))
	{
		Throw "Please ensure that DemoIISWebsite module is installed"
	}

	$Website = get-Website -Name $Name

	if($Website -eq $null)
	{
		$ensureResult = "Absent" 
	}

	elseif($Website.count -eq 1)
	{
		$ensureResult = "Present"
	}

	$getTargetResourceResult = @{
					Name = $Website.Name;
					Ensure = $ensureResult;
					PhysicalPath = $Website.physicalPath;
					State = $Website.state;
					ID = $Website.id;
					ApplicationPool = $Website.applicationPool;
					Protocol = $Website.bindings.Collection.protocol;
					Binding = $Website.bindings.Collection.bindingInformation;
				    }

	return $getTargetResourceResult
}

function Set-TargetResource
{
	[CmdletBinding(SupportsShouldProcess=$true)]
	param
	(
		[ValidateSet("Present", "Absent")]
		[string]$Ensure = "Present",

		[Parameter(Mandatory)]
		[ValidateNotNullOrEmpty()]
		[string]$Name,

		[Parameter(Mandatory)]
		[ValidateNotNullOrEmpty()]
		[string]$PhysicalPath,

		[ValidateSet("Started", "Stopped")]
		[string]$State = "Started",

		[string]$ApplicationPool,

		[string]$BindingInfo,

		[string]$Protocol
	)
	
	$Result=$psboundparameters.Remove("Ensure")
	
	$Result=$psboundparameters.Remove("State")

	$Result=$psboundparameters.Remove("BindingInfo")

	$Result=$psboundparameters.Remove("Protocol")

	if(!(Get-Module -ListAvailable -Name DemoIISWebsite))
	{
		Throw "Please ensure that WebAdministration module is installed."
	}

	$Website = get-Website -Name $Name

	if($Ensure -eq "Present") 
	{
		if($Website -eq $null)
		{
			$Website = New-Website @PSBoundParameters

			$Result = Stop-Website -Name $Name -ErrorAction Stop

				if($BindingInfo -ne $null)
				{
					$DefaultBindingInfo = $Website.bindings.Collection.bindingInformation

					$DefaultProtocol = $Website.bindings.Collection.protocol
					
					if($DefaultBindingInfo -ne $BindingInfo)
					{
						Set-WebBinding -Name $Name -BindingInformation $DefaultBindingInfo -PropertyName BindingInformation -Value $BindingInfo -ErrorAction Stop   
					}

					if(($DefaultProtocol -ne $Protocol) -and ($Protocol -ne ""))
					{
						Set-WebBinding -Name $Name -PropertyName Protocol -Value $Protocol -ErrorAction stop
					}
				}

				Start-Sleep -Seconds 1
				
				Start-Website -Name $Name -ErrorAction stop

				Write-Verbose("Successfully started website $Name.")
		} 

		else
		{
			$UpdateNotRequired = $true

			$DefaultBindingInfo = $Website.bindings.Collection.bindingInformation

			$DefaultProtocol = $Website.bindings.Collection.protocol

			if($Website.PhysicalPath -ne $PhysicalPath)
			{
				$UpdateNotRequired = $false

				Set-ItemProperty "IIS:\Sites\$Name" -Name physicalPath -Value $PhysicalPath -ErrorAction Stop

				Write-Verbose ("The physical path for the website $Name has been updated.")
			}

			if(($DefaultBindingInfo -ne $BindingInfo) -and ($BindingInfo -ne ""))
			{
				$UpdateNotRequired = $false

				Set-WebBinding -Name $Name -BindingInformation $DefaultBindingInfo -PropertyName BindingInformation -Value $BindingInfo -ErrorAction Stop
				
				Write-Verbose("Bindings for website $Name have been updated.");
			}

			if(($website.applicationPool -ne $ApplicationPool) -and ($ApplicationPool -ne ""))
			{
				$UpdateNotRequired = $false

				Set-ItemProperty "IIS:\Sites\$Name" -Name applicationPool -Value $ApplicationPool -ErrorAction Stop
			   
				Write-Verbose("Application Pool for website $Name has been updated to $ApplicationPool")
			}

			if(($DefaultProtocol -ne $Protocol) -and ($Protocol -ne ""))
			{
				$UpdateNotRequired = $false
				
				Set-WebBinding -Name $Name -PropertyName Protocol -Value $Protocol -ErrorAction stop
				
				Write-Verbose("Protocol for website $Name has been updated to $Protocol")
			}

			if(($Website.State -ne $State) -and ($State -ne ""))
			{
				$UpdateNotRequired = $false
				
				if($State -eq "Started")
				{
					$ExistingWebsite = Get-Website | where Name -ne $Name
					
					$WebsiteBindingConflict = $false
					
					foreach($site in $ExistingWebsite)
					{
						if($site.bindings.collection.bindingInformation -eq $BindingInfo)
						{
							$WebsiteBindingConflict = $true
							
							Throw ("cannot start the website $Name due to binding conflict.")
						}  
					}
					if($WebsiteBindingConflict = $false)
					{
						Start-Website -Name $Name -ErrorAction Stop
					}
				}
				else
				{
					Stop-Website -Name $Name -ErrorAction Stop
				}
				
				Write-Verbose("Successfully $State the website $Name.")
			}

			if("$UpdateNotRequired = $true")
			{
				Write-Verbose("Website $Name already exists and parameters do not need to be updated.")
			}
		}
	}

	else

	{
		if($Website -eq $null)
		{
			Write-Verbose("The Website $Name does not exist.")
		}
		else
		{
			Remove-Website -Name $Name -ErrorAction Stop
			
			Write-Verbose("Successfully removed website $Name.")
		}
	}
}

function Test-TargetResource
{
        [CmdletBinding()]
        [OutputType([System.Boolean])]
        param
        (
            [ValidateSet("Present","Absent")]
            [System.String]$Ensure,

            [parameter(Mandatory = $true)]
            [System.String]$Name,

            [parameter(Mandatory = $true)]
            [System.String]$PhysicalPath,

            [ValidateSet("Started","Stopped")]
            [System.String]$State,

            [System.String]$Protocol,

            [System.String]$BindingInfo,

            [System.String]$ApplicationPool
        )

        $DesiredConfigurationMatch = $true
        
        if(!(Get-Module -ListAvailable -Name DemoIISWebsite))
        {
            Throw "Please ensure that WebAdministration module is installed."
        }
 
        $Website = get-Website -Name $Name

        if(($Ensure -eq "Present" -and $Website -eq $null) -or ($Ensure -eq "Absent" -and $Website -ne $null))
        {
            $DesiredConfigurationMatch = $false
            
            Write-Verbose("The Ensure State of the website $Name doesn't match with the Desired Configuration State.")
        }

        if($Website -ne $null)
        {
            if($Website.PhysicalPath -ne $PhysicalPath)
            {
            $DesiredConfigurationMatch = $false
            
            Write-Verbose("The Physical Path of the website $Name doesn't match with the Desired Configuration state.")
			}

            if(($Website.State -ne $State) -and ($State -ne ""))
            {
            $DesiredConfigurationMatch = $false
            
            Write-Verbose("The State of the website $Name doesn't match with the Desired Configuration state.")
			}

			if(($website.applicationPool -ne $ApplicationPool) -and ($ApplicationPool -ne ""))
			{
				$DesiredConfigurationMatch = $false
			
				Write-Verbose("The Application pool of the website $Name doesn't match with the Desired Configuration State.")
			}

			if(($DefaultProtocol -ne $Protocol) -and ($Protocol -ne ""))
			{
				$DesiredConfigurationMatch = $false
			
				Write-Verbose("The Protocol of the website $Name doesn't match with the Desired Configuration State.")
			}

			if(($DefaultBindingInfo -ne $BindingInfo) -and ($BindingInfo -ne ""))
			{
				$DesiredConfigurationMatch = $false
			
				Write-Verbose("The Binding Information of the website $Name doesn't match with the Desired Configuration State.")
			}
        }

        return $DesiredConfigurationMatch
    }