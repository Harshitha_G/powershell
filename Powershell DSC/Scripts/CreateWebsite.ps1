﻿Configuration CreateWebsite
{
    Import-DscResource -Module DemoIISWebsite
    
    node("localhost")
    {
        Website CreateWebsite
        {
            Name = "NewWebsite"
            Ensure = "Present"
            PhysicalPath = "E:\New folder"
            State = "Started"
            BindingInfo = "192.168.56.1:443:"
            ApplicationPool = "DefaultAppPool"
            
        }
    }
}

CreateWebsite -OutputPath "F:\PS Assignments\Powershell DSC\MOF\CreateWebsite"

Publish-DscConfiguration -Verbose -Path "F:\PS Assignments\Powershell DSC\MOF\CreateWebsite"

Start-DSCConfiguration -Verbose -Wait -Force -Path "F:\PS Assignments\Powershell DSC\MOF\CreateWebsite"

