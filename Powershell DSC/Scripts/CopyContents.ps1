﻿param(
    [parameter(Mandatory=$true)][string]$Source ,
    [parameter(Mandatory=$true)][string]$Destination
)
Configuration CopyDirectoryContents
{
    Import-DscResource -ModuleName PSDesiredStateConfiguration
    
    node("localhost")
    {
        File CopyDirectory
        {
            Ensure = "Present"
            SourcePath = "$Source"
            DestinationPath = "$Destination"
            Type = "Directory"
            Recurse = $true
            MatchSource = $true
        }
    }
}

CopyDirectoryContents -OutputPath "F:\PS Assignments\Powershell DSC\MOF\CopyDirectoryContents"

Publish-DscConfiguration -Verbose -Path "F:\PS Assignments\Powershell DSC\MOF\CopyDirectoryContents"

Start-DSCConfiguration -Verbose -Wait -Force -Path "E:\PS Assignments\Powershell DSC\MOF\CopyDirectoryContents"