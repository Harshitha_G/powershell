﻿Configuration CreateFile
{
    Import-DscResource -ModuleName PSDesiredStateConfiguration
    
    node("localhost")
    {
        File CreateFile
        {
            Ensure = "Present"
            DestinationPath = "F:\PS Assignments\Powershell DSC\NewFile.txt"
            Type = "File"
            Contents = "New file created via powershell DSC "
        }
    }
}

CreateFile -OutputPath "F:\PS Assignments\Powershell DSC\MOF\CreateFile"

Publish-DscConfiguration -Verbose -Path "F:\PS Assignments\Powershell DSC\MOF\CreateFile"

Start-DSCConfiguration -Verbose -Wait -Force -Path "F:\PS Assignments\Powershell DSC\MOF\CreateFile"