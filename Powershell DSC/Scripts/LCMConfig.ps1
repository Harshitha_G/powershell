﻿[DscLocalConfigurationManager()]

Configuration LCMConfig
{
    Node localhost
    {
        Settings
        {
            ConfigurationModeFrequencyMins = 15

            ConfigurationMode = "ApplyAndAutoCorrect"

            RefreshMode = "Push"
        }
    }
}

LCMConfig -OutputPath "F:\PS Assignments\Powershell DSC\MOF\LCMConfig"

Set-DscLocalConfigurationManager -Path "F:\PS Assignments\Powershell DSC\MOF\LCMConfig"