﻿[DscLocalConfigurationManager()]

Configuration PartialConfiguration
{
    Node localhost
    {
        Settings
        {
            ConfigurationModeFrequencyMins = 15

            ConfigurationMode = "ApplyAndAutoCorrect"

            RefreshMode = "Push"
        }
        PartialConfiguration CopyDirectoryContents
        {
            Description = 'Configuration to create a destination folder and copy the contents of source folder to it '
            RefreshMode = 'Push'
        }
        PartialConfiguration CreateFile
        {
            Description = 'Configuration to create a new file after "CopyDirectoryContents" Configuration is applied'
            RefreshMode = 'Push'
        }
        PartialConfiguration ChangeSystemTimeZone
        {
            Description = 'Configuration to Change the System Time Zone by taking user parameter'
            RefreshMode = 'Push'
        }
    }
}

PartialConfiguration -OutputPath "F:\PS Assignments\Powershell DSC\MOF\LCMPartialConfig"

Set-DscLocalConfigurationManager -Path "F:\PS Assignments\Powershell DSC\MOF\LCMPartialConfig"

Start-DscConfiguration -UseExisting