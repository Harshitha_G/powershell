﻿Configuration ChangeSystemTimeZone
{
    Import-DSCResource -ModuleName xTimeZone
    
    node("localhost")
    {
        xTimeZone ChangeSystemTimeZone 
        { 
            TimeZone = "Mountain Standard Time"  
            IsSingleInstance = "Yes"
        }
    }
}
 
ChangeSystemTimeZone -OutputPath "F:\PS Assignments\Powershell DSC\MOF\ChangeSystemTimeZone"

Publish-DscConfiguration -Verbose -Path "F:\PS Assignments\Powershell DSC\MOF\ChangeSystemTimeZone"

Start-DSCConfiguration -Verbose -Wait -Force -Path "F:\PS Assignments\Powershell DSC\MOF\ChangeSystemTimeZone"