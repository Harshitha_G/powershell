﻿param(
    [parameter(Mandatory=$true)][int32]$InputYear 
)

#Check if given year is leapyear
if($InputYear%100 -eq 0)
{
    if($InputYear%400 -eq 0)
    {
        Write-Output "$InputYear is a LeapYear"
    }
    else
    {
        Write-Output "$InputYear is not a Leap Year"
    }
}
else
{
    if($InputYear%4 -eq 0)
    {
        Write-Output "$InputYear is a LeapYear"
    }
    else
    {
        Write-Output "$InputYear is not a Leap Year"
    }
}