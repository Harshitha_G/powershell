﻿param
(
    [parameter(Mandatory=$true)][int32]$DecimalInput ,
    [parameter(Mandatory=$true)][int32]$BinaryInput 
)

#To convert decimal to binary

Write-Output "Binary Value of $DecimalInput is"
[convert]::ToString("$DecimalInput" ,2)

#To convert binary t decimal

Write-Output "Decimal Value of $BinaryInput is "
[convert]::ToInt32("$BinaryInput" ,2) 

