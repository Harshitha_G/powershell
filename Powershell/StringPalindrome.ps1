﻿param(
    [parameter(Mandatory=$true)][string]$InputString
)


#Check if input string is palindrome or not

$InputCharArray = $InputString.ToCharArray()

[array]::Reverse($InputCharArray)

$StringReverse= -join($InputCharArray)

Write-Output $StringReverse

if($InputString -eq $StringReverse)
    {
    Write-Output "The Input String is Palindrome"
    }

else
    {
    Write-Output "The Input String is Not Palindrome"
    }

