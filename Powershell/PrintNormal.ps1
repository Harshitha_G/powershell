﻿#Read a file from starting and print each line

$data = Get-Content "C:\Users\intime\Documents\notes.txt"
write-host "$data.count total lines read from file"
foreach ($line in $data)
{
    write-host $line
}


<#
$filename="C:\Users\intime\Documents\notes.txt"
foreach ($line in [System.IO.File]::ReadLines($filename)) {
    Write-Output $line
}
#>
